# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.40.0"
  constraints = "~> 3.40.0"
  hashes = [
    "h1:09F9Q62aeWgIVEuAqA9WafkvSzHdvDUCpFni0X3GVyY=",
    "zh:1034d628bfb6c751abce884a2e7035abd2e05a2f944818ead2bc53bdf8f33914",
    "zh:4285e594dde25ec948b4e5a73f727769d429758a7b4e8b9bcb66fdb51c68a73f",
    "zh:83ed507eebd4c05a3bb2508956bba68ac1336aa3a7778256d131aa388aaa905d",
    "zh:98fba5b49bf6a686cabc4fe5d187372849936bab279672bb3de466cf28823416",
    "zh:9e08989bc04a5e3ca0ebc2aca634f1a873105465532f361e1d3382a5b11d4fa5",
    "zh:b781f2bec0f9d7375fbdb769ed151c2ddff33231c148609a18d45b74eb0f7371",
    "zh:bf1432de90f1a856c7d895498cc40944bc13f58af5045bb43724595259cd1d11",
    "zh:c38ec5aa03aed5dae49062c30691722fa49e0c5ac1e730c635c69e927b0c6845",
    "zh:d4eb990842a95fef449a4bf6aec625268fa963fd1ebc793060acd0e10512628a",
    "zh:daac70787ad80c760e24da74b7b0f4b2db9414b92347a1ce95aa3a5a3e1261d3",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.0.0"
  hashes = [
    "h1:grDzxfnOdFXi90FRIIwP/ZrCzirJ/SfsGBe6cE0Shg4=",
    "zh:0fcb00ff8b87dcac1b0ee10831e47e0203a6c46aafd76cb140ba2bab81f02c6b",
    "zh:123c984c0e04bad910c421028d18aa2ca4af25a153264aef747521f4e7c36a17",
    "zh:287443bc6fd7fa9a4341dec235589293cbcc6e467a042ae225fd5d161e4e68dc",
    "zh:2c1be5596dd3cca4859466885eaedf0345c8e7628503872610629e275d71b0d2",
    "zh:684a2ef6f415287944a3d966c4c8cee82c20e393e096e2f7cdcb4b2528407f6b",
    "zh:7625ccbc6ff17c2d5360ff2af7f9261c3f213765642dcd84e84ae02a3768fd51",
    "zh:9a60811ab9e6a5bfa6352fbb943bb530acb6198282a49373283a8fa3aa2b43fc",
    "zh:c73e0eaeea6c65b1cf5098b101d51a2789b054201ce7986a6d206a9e2dacaefd",
    "zh:e8f9ed41ac83dbe407de9f0206ef1148204a0d51ba240318af801ffb3ee5f578",
    "zh:fbdd0684e62563d3ac33425b0ac9439d543a3942465f4b26582bcfabcb149515",
  ]
}
