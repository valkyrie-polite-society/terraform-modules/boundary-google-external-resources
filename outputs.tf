output "key_ring" {
  value = google_kms_key_ring.boundary
}

output "root_kms_key" {
  value = google_kms_crypto_key.root
}

output "worker_auth_kms_key" {
  value = google_kms_crypto_key.worker_auth
}

output "recovery_kms_key" {
  value = google_kms_crypto_key.recovery
}

output "config_kms_key" {
  value = google_kms_crypto_key.config
}

output "postgres_instance" {
  value = google_sql_database_instance.boundary
}

output "postgres_user" {
  value = google_sql_user.boundary
}
