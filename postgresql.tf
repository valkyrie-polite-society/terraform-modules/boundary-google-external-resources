resource "google_sql_database_instance" "boundary" {
  name             = "${var.cluster_name}-postgresql"
  region           = var.region
  database_version = "POSTGRES_12"

  settings {
    tier = "db-f1-micro"
  }

  lifecycle {
    ignore_changes  = [settings.0.replication_type]
  }
}

resource "google_sql_database" "boundary" {
  name     = "boundary"
  instance = google_sql_database_instance.boundary.name
}


resource "google_sql_user" "boundary" {
  name     = "boundary"
  instance = google_sql_database_instance.boundary.name
  password = random_password.postgresql.result
}

resource "random_password" "postgresql" {
  length  = 32
  special = false
}
